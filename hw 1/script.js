
// прототипное наследование в Javascript это возможность содать новый обьект на основе существующего, сохрание нужные свойства  

class Employee{
    constructor(name, age, salary){
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name;
    }
    set name(newName) {
        this._name = newName;
    }

    get age() {
        return this._age;
    }
    set age(newAge) {
        this._age = newAge;
    }

    get salary() {
        return this._salary;
    }
    set salary(newSalary) {
        this._salary = newSalary;
    }

    showInfo() {
        console.log(`Имя - ${this._name}. Возраст - ${this.age}. ЗП - ${this.salary}`)
    }
  
}

class Programmer extends Employee{
    constructor(name, age, salary, lang){
        super(name, age, salary);
        this.lang = lang;
    }

    get salary(){
        return this._salary * 3
    }
    showInfo() {
        console.log(`Имя - ${this._name}. Возраст - ${this.age}. ЗП - ${this.salary}. Языки - ${this.lang}`)
    }
}

const newEmployeeAnna = new Employee('Anna', '27', '2000').showInfo();
const newEmployeeRoman = new Employee('Roman', '29', '3000').showInfo();

const newProgrammerOlga = new Programmer('Olga', '20', '4000', 'JS').showInfo();
const newProgrammerDima = new Programmer('Dima', '25', '5000', 'HTML, CSS').showInfo();
