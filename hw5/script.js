
const requestURL = 'https://api.ipify.org/?format=json'

async function getUserIp(){
    const {ip} = await fetch(requestURL).then(res => res.json());
    console.log(ip)
    getUserAddress(ip)
}

async function getUserAddress(ip){
    const result = await fetch(`http://ip-api.com/json/${ip}?fields=continent,country,regionName,city`).then(res => res.json());
    console.log(result);
    renderUserInfo(result)
}

function renderUserInfo(address){
    const div = document.querySelector('.infoContainer')
    div.insertAdjacentHTML('beforeend', 
    `<p>Континент: ${address.continent}</p>
    <p>Страна: ${address.country}</p>
    <p>Регион: ${address.regionName}</p>
    <p>Город: ${address.city}</p>
    `)
    div.classList.add('infoContainerStyle')
    button.setAttribute('disabled', true);
}

const button = document.querySelector('.button');
button.addEventListener('click', getUserIp)
